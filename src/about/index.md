# About

Both, *Split* and *The Beast Desktop* are developed in the spare time of user
**kevcrumb**. While I develop the former to support your privacy and
sovereignty, the latter is meant to allow maximum speed and efficacy for your
daily computer use.

## Split

Split is as stable as its parent, Void Linux. It can function as your daily
driver.

It extends the Void Linux Live disk with functionality to setup and manage LXC
containers that are used to start graphical interfaces, simultanously, on
separate TTYs.

Every container gets separate Tor circuits to the outside world, which prevents
the TTYs from being correlated to one another. Thus, in a Split context, the
containers are referred to as *"identities"*.

## Beast

Beast builds on dwm, the window manager that is so stable, it rarely gets
updates. It doesn't need them.

Beast aims to give you the best tool for each job and hands you scripts for
optimized workflows. A handful of these scripts are already included. Hundreds
more are ready and waiting to pass quality-control. They will be hand-selected
and added as time permits.

## A Note About Development

Development will typically happen in bursts, depending on my personal schedule.
Expect periods of several months without new features being added to Split or
Beast.

But don't worry: In case of Split you do not need the newest packages - it
mostly provides the Kernel for your identities - and in Beast you will be using
Void Linux packages directly anyway.
