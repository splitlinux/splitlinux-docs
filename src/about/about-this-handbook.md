# About This Handbook

Documenting Split and The Beast Desktop has just begun.

*It is one of the most time-consuming parts. If you want to help, look at any
yet-undocumented script and describe it's function for inclusion in this
manual.*

A local copy of this handbook, in several formats, can be installed via the
`splitlinux-docs` package and accessed from the `/usr/share/doc/splitlinux/`
directory structure.

The purpose of this document is to explain how to setup, configure, and use
Split Linux systems.

To search for a particular term within the Handbook, select the 'magnifying
glass' icon, or press 's'.

## Example Commands

Examples in this guide may have snippets of commands to be run in your shell.
When you see these, any line beginning with `$` is run as your normal user.
Lines beginning with `#` are run as `root`. After either of these lines, there
may be example output from the command.

### Placeholders

Some examples include text with placeholders. Placeholders indicate where you
should substitute the appropriate information. For example:

```
# ln -s /etc/sv/<service_name> /var/service/
```

This means you need to substitute the text `<service_name>` with the actual
service name.
