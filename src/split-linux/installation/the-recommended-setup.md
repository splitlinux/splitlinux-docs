# The Recommended Setup

At the current stage Split Linux is still bare bones. This guides walk you from the initial hard disk preparation all the way through to starting your first container session.

At the beginning of every essential section there's a link to a screencast that may help spot potential errors in your process.

If you're running into any issues come and ask your questions over at our Reddit [r/splitlinux](https://splitlinux.org).


## Hard disk preparation

1. Create two primary partitions on your hard disk using your preferred partitioning tool (`cfdisk /dev/<DEVICE>`). The first is reserved for a decoy OS. Use the second for Split.
2. Command `format_for_splitlinux /dev/<DEVICE>` where "`<DEVICE>`" is the path to the disk you want to use.
3. Follow the instructions on the screen. Remember the password you set.
4. When finished follow the final suggestions on how to continue.

*If you are interested in the manual steps involved, you may [watch this ASCII Cast about the hard disk preparation](https://asciinema.org/a/yJECPqPL6Dd7YYVs52eqi1UnC).*


## Container setup

Boot your system from the Split Linux pendrive. Split will detect the partition you just created, ask for its password and mount it.

1. Switch to the second terminal (<kbd>Ctrl+Alt+F2</kbd>) and log in as **root**.
2. Command `create_voidlinux_container v 122` where "v" is the desired name of the container/identity and "122" an ID in the range 100-254.
3. Follow the instructions on the screen. Remember the password you set.
4. When finished run the suggested command to install the Beast desktop environment. Name any additional packages you want at the end of the `xbps-install` command.

When you now log out and log in using the container name as user name (eg **v**) you should end up *within* the container's graphical environment.

And this is where the journey begins.

*Continue at [Configuration: Internet for Applications](../configuration/internet-for-applications.html) to learn how to connect your applications through Tor.*
