# Installation

Split Linux is run as a Live OS which mounts an encrypted hard disk containing
LXC containers.

Please refer to <https://splitlinux.org> for basic setup instructions.
