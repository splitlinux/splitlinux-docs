# Creating A Custom Container (Optional)

_This explains manual container creation. You should only use it if the [automated process](./the-recommended-setup.md) doesn't satisfy your needs._


## Container setup

[***Click here to watch the steps for "Container setup" as ASCII Cast***](https://asciinema.org/a/XExyeOnqvPqZIfWu9Om56tzho)

Boot your system from the Split Linux pendrive. Split will detect the partition you just created, ask for its password and mount it.

1. Switch to the second terminal (<kbd>Ctrl+Alt+F2</kbd>) and log in as **root**. *(Currently, the first terminal displays Tor debug output which renders it unfit for interactive use. This will be changed in a future release.)*
2. Ensure that **horde** is mounted (`mount | grep split-horde`) and that you're connected to the Internet (`ping -c1 splitlinux.org`).
3. Create a container. *You can replace name and amend the last row of parameters to install a different distro as offered at [images.linuxcontainers.org](https://images.linuxcontainers.org/).*
   <pre>$ lxc-create --config /usr/share/splitlinux/config/splitlinux-default.conf \
                --name v --template download -- \
                --release current --dist voidlinux --arch amd64 --variant musl</pre>
4. Remove superfluous includes from newly created container configuration (`sed 's#^lxc.*lxc/config.*##g' /var/lib/lxc/v/config`).
5. Set a unique IP address for the container (`sed -i 's#100#122#g' /var/lib/lxc/v/config`).
6. Start the container (`lxc-start v`) and step into it (`lxc-attach v`).
7. In the container create a user. Its name must match the container name! (`useradd --create-home v`).
8. Set a password for that user (`passwd v`).

*Our example names the container **v** as in Void Linux. Using single letters for container- and user names is generally a great way to save time typing.*


## Container package installation

[***Click here to watch the steps for "Container package installation" as ASCII Cast***](https://asciinema.org/a/yWT2kvikmVn3uQLJC2hWMqrba)

1. Make the container use Void's Tor mirror for packages (`echo 'repository=http://lysator7eknrfl47rlyxvgeamrv7ucefgrrlhk7rouv3sna25asetwid.onion/pub/voidlinux/current/musl' > /etc/xbps.d/00-repository-main.conf`).
2. Fetch the package index (`SOCKS_PROXY="socks5://172.18.0.2:9050" xbps-install -Su`).
3. Install a basic graphical environment (`SOCKS_PROXY="socks5://172.18.0.2:9050" xbps-install -S dwm st xorg-minimal xorg-fonts monero curl torsocks`).
4. Configure **dwm** to start as graphical environment (`echo 'exec dwm' >> /home/v/.xinitrc`).
5. Exit the container (`exit`) and verify that you're back in the host system (`hostname`).
6. Reboot (`reboot`).


### Notes

* A window manager or a desktop environment (like **dwm**) is highly recommended if you want a graphical interface.
* If **xorg-minimal** is not installed, dwm will fail to start complaining about `DISPLAY` not being set.
* If **xorg-fonts** is not installed, dwm will fail to start complaining that `monospace:10` is missing.
* **torsocks** is essential as it allows pretty much any application to route through Tor.

When you now log in using the container name as user name (eg **v**) you should end up *within* the container's graphical environment.

*Continue at [Configuration: Internet for Applications](../configuration/internet-for-applications.md) to learn how to connect your applications through Tor.*
