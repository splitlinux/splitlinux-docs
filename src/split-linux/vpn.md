# Virtual Private Networks

In order for a VPN to be compatible with Split's protected routing setup it must
allow connection from Tor.

```
Like so:
you -> tor -> VPN -> internet

NOT so:
you -> VPN -> tor -> internet
```

It must be possbile to establish a connection after adding `socks-proxy
172.XX.0.2 9050` to the OpenVPN config file, where the "XX" octet depends on the
type of container (isolated/leaky/exposed) you are using.

## Why a VPN?

Split Linux torifies internet connections. Many parties, while not completely
blocking connections via Tor, still make them very hard to access. You may end
up solving countless loops of reCAPTCHA only to be denied access anyways or DDoS
protection leaves you hanging at their check for validity.

The best way to deal with this is to avoid such sites altogether, but when
there's no alternative provider of a service yet, or when you don't want to play
into the "something to hide" narrative, it's helpful to appear as yet another
citizen.

## Commercial Providers

This is a list of commercial VPN providers that did not block our connection
from Tor and let us surf the web normally. Please note the date and be aware
that policies may change.

| Provider                                                                                                                                               | Comment                                                           | Cost (1y) | Checked  |
|--------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------|-----------|----------|
| [AirVPN](https://airvpn.org/?referred_by=424127) ([onion](https://airvpn3epnw2fnsbx5x2ppzjs6vxtdarldas7wjyqvhscj7x43fxylqd.onion/?referred_by=424127)) | price when on offer                                               | 31.85 EUR | Nov-2022 |
| [IVPN](https://www.ivpn.net)                                                                                                                           |                                                                   | 60.00 USD | Feb-2023 |
| [SnowHaze](https://snowhaze.com)                                                                                                                       | only their "Firewall Bypass", Germany-location, confirmed working | 50.00 CHF | Nov-2022 |
| [VPN.ac](https://vpn.ac)                                                                                                                               | 45$/y when paying for two years; start with the [1-week trial](https://vpn.ac/cart.php?a=add&pid=4) to receive the best rate afterwards | 58.00 USD | Apr-2024 |
| [Xeovo](https://xeovo.com/?r=g6UYaG9y9a6Q)                                                                                                             | support claims it's working - to be confirmed                     | 35.88 EUR | Nov-2022 |

If you know further VPN's that work, please let us know. A good starting point
for finding VPN's is the list at <https://monerica.com>.

### Incompatible

Most providers deny VPN through Tor and are thus incompatible with a secure setup. Examples:

- Mullvad (tested 01-2024)
- ProtonVPN

## Homemade

Tech-savvy users can turn any VPS hosting to use as VPN. As a major advantage
this means that your traffic will look even more generic, since you do not surf
using any of the known IP addresses associated with a VPN provider. If said VPS
is on a dedicated IP, it might be a downside to have all traffic tied to one
unique user.

While pretty much every "Big Tech" company offers a free-tier hosting, it's
counter-productive to empower those by handing over even more statistics to
analyze.

Know that you can find great offers for roughly one dollar a month, some of
which payable with Monero (through BTC gateways).
