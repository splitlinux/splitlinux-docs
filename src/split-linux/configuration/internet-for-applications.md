# Internet for Applications

If an application directly supports SOCKS proxies you can pass it the IP of the Tor router directly by whatever means the application supports.

Some, like the Void package manager XBPS support the `SOCKS_PROXY` environment variable. Others, like Monero, will take the proxy IP as command line argument.

## torsocks (for any application)

If an application lacks direct SOCKS support, the `torsocks` command can usually be used to transparently route that application's traffic through Tor anyway.

Just prepend a command with "`torsocks -a 172.18.0.2 -P 9050 --isolate`".


## Container connection test

If your host is correctly connected to the Internet you should be able to establish a connection via Tor. Try it:

1. Bring up a terminal (<kbd>Alt+Shift+Enter</kbd>) and check your Tor IP by telling `curl` to use the Tor router as proxy:
   <pre>$ curl --proxy socks5h://172.18.0.2:9050 https://check.torproject.org/api/ip</pre>
2. Check your Tor IP transparently by wrapping `curl` within the `torsocks` command:
   <pre>$ torsocks -a 172.18.0.2 -P 9050 --isolate curl https://check.torproject.org/api/ip</pre>

Note that your IP displays differently on every invocation as every connection establishes a new Tor circuit.


---

## Further examples

### Ansible

    $ ANSIBLE_SSH_ARGS="-o ProxyCommand='openbsd-nc -X 5 -x 172.18.0.2:9050 %h %p'" ansible-playbook -i hosts playbook.yaml

See footnotes ansible[^ansible], cows[^cows]


### Chromium

    $ chromium --proxy-server=socks://172.18.0.2:9050


### Git

    $ GIT_SSH_COMMAND="ssh -o ProxyCommand='nc --proxy-type socks5 --proxy 172.18.0.2:9050 %h %p'" git push origin master:master

See footnotes git[^git], ssh[^ssh]


### Monero

    $ monero-wallet-cli --proxy 172.18.0.2:9050 --untrusted-daemon --daemon-host 4egylyolrzsk6rskorqvocipdo4tqqoyzxnplbjorns7issmgpoxvtyd.onion

See footnote monero[^monero]


### Rsync over SSH

    $ rsync --rsh 'ssh -o ProxyCommand="nc --proxy-type socks5 --proxy 172.18.0.2:9050 %h %p" -va /tmp/FILE user@host:/tmp/ [^ssh]

See footnote ssh[^ssh]


### Secure Shell

    $ ssh -o ProxyCommand='nc --proxy-type socks5 --proxy 172.18.0.2:9050 %h %p' user@host

See footnote ssh[^ssh]


### Tor Browser

![Connecting Tor Browser](../../images/connecting_tor-browser.png)


### youtube-dl

    $ youtube-dl --proxy socks5://172.18.0.2:`shuf -n1 -i 9050-9059` <VIDEO_ID>


### XBPS

    $ SOCKS_PROXY=socks5://172.18.0.2:9050 xbps-install -Su

---


* [^ansible]: For Ansible, nmap's netcat often fails with UNREACHABLE: **Failed to connect to the host via ssh: kex_exchange_identification: banner line contains invalid characters\r nbanner exchange: Connection to UNKNOWN port 65535: invalid format**. `openbsd-nc` (package `openbsd-netcat`) doesn't seem to trigger such errors.
* [^cows]: For readability you will probably want to call `ansible-playbook` with the `ANSIBLE_NOCOWS=1` environment variable set.
* [^git]: If this fails and a warning regarding LFS is displayed, try disabling it as instructed.
* [^ssh]: These examples use the `netcat` variant provided by the `nmap` package.
* [^monero]: See [monero.fail](https://monero.fail) for a list of Tor remote nodes.
