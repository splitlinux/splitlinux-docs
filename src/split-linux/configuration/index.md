# Configuration

## Overrides

Overrides enable users to customize the configuration of the live environment
(i.e. the Split host system).

Thanks to overrides, one can boot vanilla Split Linux as intended (as Live OS)
and still be able to mount additional devices (*fstab*), execute arbitrary
commands on boot (*rc.local*), manage access to devices (*udev/rules.d/*) and
more.

### Usage

Any file placed into `/var/lib/lxc/_host/overrides/etc/` will replace the
corresponding file in `/etc/` upon boot (subdirectories supported).

For example, to activate the amount of huge pages required for optimum
Monero-mining performance set the corresponding sysctl value and reboot:

```
# cd /var/lib/lxc && mkdir -p _host/overrides/etc/
# echo 'vm.number_hugepages=1280' >> /var/lib/lxc/_host/overrides/etc/sysctl.conf
# reboot
```
