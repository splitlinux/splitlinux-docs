# Summary

- [About](./about/index.md)
   - [About This Handbook](./about/about-this-handbook.md)
- [Split Linux](./split-linux/index.md)
   - [Installation](./split-linux/installation/index.md)
      - [The Recommended
         Setup](./split-linux/installation/the-recommended-setup.md)
      - [Creating a custom
         Container](./split-linux/installation/creating-a-custom-container.md)
   - [Configuration](./split-linux/configuration/index.md)
      - [Internet for
         Applications](./split-linux/configuration/internet-for-applications.md)
   - [VPN](./split-linux/vpn.md)
- [The Beast Desktop](./the-beast-desktop/index.md)
   - [Beast dwm](./the-beast-desktop/beast-dwm/index.md)
   - [Beast Scripts](./the-beast-desktop/beast-scripts/index.md)
      - [Mine Monero](./the-beast-desktop/beast-scripts/BDmine_monero.md)
      - [Merge skel](./the-beast-desktop/beast-scripts/BMmerge_skel.md)
   - [Beast skel](./the-beast-desktop/beast-skel/index.md)
   - [Beast CommandLine](./the-beast-desktop/beast-commandline/index.md)
   - [Beast Extras](./the-beast-desktop/beast-extras/index.md)
