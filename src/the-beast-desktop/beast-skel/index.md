# Beast skel

The package **beast-skel** provides optimized defaults for Beast Desktop
applications.

When an identity is first created, the contents of `/etc/skel` are copied into
the main user's HOME directory.

## Updates

Changes in **beast-skel** only affect the original files in `/etc/skel`.

Whenever the package is updated, it makes sense to merge the provided
optimizations into the user's configuration.

The maintenance script [BMmerge_skel](../beast-scripts/BMmerge_skel.md)
simplifies the task.
