# Mine Monero

    cmd: BDmine_monero
    kbd: Shift+Pause

Every time you want to donate hash rate to Split Linux, simply lock the screen
using <kbd>Shift+Pause</kbd>.

To start donating from the command line, command `BDmine_monero` without any arguments.


If you want to mine to your own account run `BDmine_monero` providing a **handle** of
your choosing and a wallet **address** as arguments. Example:

    $ BDmine_monero mary 847vSaDnoiFehRQLwfnETRiEQwPWxzWWyeJtw6o3k1DUTvrfD2DCoHUKjpZ7Ui1PoQ5JcbdcEcpVcAiQYDA2Vyda8EsAbzX

If the handle is already associated with a different wallet address, the miner
will exit with a message indicating this.

After the command ran successfully once, handle and address are linked and you
can leave out the wallet address on future invocations.

_Note: The miner will only be able to connect if a working default network route
for the internet connection is active (check `ip route`)._


## Tweaking

### Hugepages

Activating hugepages is crucial for efficient mining. Use Split Linux [Overrides](../../split-linux/configuration/index.md#overrides)
to persistently define `vm.number_hugepages=1280` in `/etc/sysctl.conf`.


### CPU threads

The amount of CPU threads used defaults to the amount of processing units
available. On a typical system, the amount of units available equals the amount
of CPU cores.

Set the environment variable `BEAST_MINER_THREADS` to override the value, ideally
in `~/.xinitrc.d/50-custom.sh` as `export BEAST_MINER_THREADS=<number>`.
