# Merge skel

```
cmd: BMmerge_skel
kbd: <none>
```

For an optimum desktop experience run `BMmerge_skel` whenever the **beast-skel**
package gets updated. This pulls new optimizations into your identity's
configuration.

`sdiff(1)`'s interactive mode is used to process any changed files. Be sure to
familiarize yourself with the tool beforehand.
