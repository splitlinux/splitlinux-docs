# Split Docs

Welcome to the Split documentation. This repository contains the source data
behind <https://docs.splitlinux.org/>. Contributing to this repository follows
the same protocol as the packages tree. For details, please read
[CONTRIBUTING](./CONTRIBUTING.md).

## Building

The [res/build.sh](./res/build.sh) script builds HTML, roff and PDF versions of
the Split documentation. It requires the following Split packages:

- `mdBook`
- `findutils`
- `lowdown` (version 0.8.1 or greater)
- `texlive`
- `perl`
- `perl-File-Which`
- `perl-JSON`
- `librsvg-utils`
- `python3-md2gemini`

## Checking

For checks and formatting, additionally install `mdbook-linkcheck` and `vmdfmt`.
